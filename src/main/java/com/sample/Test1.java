// package com.sample;

// import java.text.SimpleDateFormat;
// import java.time.LocalDate;
// import java.time.format.DateTimeFormatter;
// import java.util.Date;

// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
// import org.testng.annotations.AfterTest;
// import org.testng.annotations.BeforeTest;
// import org.testng.annotations.Test;

// import io.qameta.allure.Epic;
// import io.qameta.allure.Feature;
// import io.qameta.allure.Severity;
// import io.qameta.allure.SeverityLevel;
// import io.qameta.allure.Step;
// import io.qameta.allure.Story;

// /**
//  * Hello world!
//  *
//  */
// @Epic("JFS")
// @Feature("Test Onboarding Flow")
// public class Test1 {

//     private static final Logger LOG = LoggerFactory.getLogger(Test1.class);
//     String username = "";
//     String password = "";

//     @BeforeTest(description = "Setup the basic env")
//     public void setup() {
//         LOG.info("Setup..");
//     }

//     @Test(description = "This is a sample test")
//     @Story("Base support for bdd annotations")
//     @Severity(SeverityLevel.CRITICAL)
//     public void enterUsername() {
//         LOG.info("Perform Login");
//         String timestamp = "1585210105";
//         SimpleDateFormat ss = new SimpleDateFormat("yyyy-MM-dd");
//         // System.out.println(ss.format(date));

//     }

//     @Test(description = "This is a sample test")
//     public void enterPassword() {
//         LOG.info("Perform Logout");
//     }

//     @AfterTest(description = "TearDown will close the things")
//     public void tearDown() {
//         LOG.info("Stop Execution..");
//     }

// }
