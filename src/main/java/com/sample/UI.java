// package com.sample;

// import java.util.Objects;

// import org.openqa.selenium.WebDriver;
// import org.openqa.selenium.chrome.ChromeDriver;
// import org.openqa.selenium.chrome.ChromeOptions;
// import org.testng.annotations.AfterClass;
// import org.testng.annotations.BeforeClass;
// import org.testng.annotations.Test;

// import io.github.bonigarcia.wdm.DriverManagerType;
// import io.github.bonigarcia.wdm.WebDriverManager;

// public class UI {

//     private WebDriver driver = null;
//     private ChromeOptions options = null;

//     @BeforeClass
//     public void setup(){

//         // SETUP DRIVER
//         WebDriverManager.getInstance(DriverManagerType.CHROME).setup();

//         options = new ChromeOptions();
//         options.addArguments("--headless");
//         options.addArguments("--disable-gpu");

//         driver = new ChromeDriver(options);

//         driver.get("https://www.google.com");
//         driver.manage().window().maximize();

//     }

//     @Test
//     public void verify(){
//         System.out.println(driver.getTitle());
//     }

//     @AfterClass
//     public void tearDown(){
//         if(Objects.nonNull(driver)){
//             driver.quit();
//         }
//     }

// }