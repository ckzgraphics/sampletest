package com.sample;

import java.io.File;
import java.io.FileWriter;

import org.testng.annotations.Test;

public class ArtifactTest {

    @Test
    public void generateFile() {

        System.out.println("STARTED..");
        String BASE_PATH = System.getProperty("user.dir");
        String RESOURCE = BASE_PATH.concat(File.separator).concat("resource").concat(File.separator);
        System.out.println("RESOURCE :: " + RESOURCE);
        try {
            File file = new File(RESOURCE.concat("sample.txt"));
            FileWriter fr = new FileWriter(file);
            fr.write("Hello File");
            fr.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("END");

    }

}